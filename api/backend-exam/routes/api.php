<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// All routes that will be inside is for public and is good for different origin resources and will be returning json response always
Route::group(['middleware' => ['cors', 'json.response']], function () {
    //Users
    Route::post('/login', 'App\Http\Controllers\Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register','App\Http\Controllers\Auth\ApiAuthController@register')->name('register.api');
    //Posts
    Route::get('/posts', 'App\Http\Controllers\PostController@index');
    Route::get('/posts/{post}', 'App\Http\Controllers\PostController@show');
    //Comments
    Route::get('/posts/{post}/comments', 'App\Http\Controllers\CommentController@index');

    // All routes to be protected must be in here
    Route::middleware('auth:api')->group(function () {
        //Posts
        Route::post('/posts', 'App\Http\Controllers\PostController@store');
        Route::patch('/posts/{post}', 'App\Http\Controllers\PostController@update');
        Route::delete('/posts/{post}', 'App\Http\Controllers\PostController@destroy');
        //Comments
        Route::post('/posts/{post}/comments', 'App\Http\Controllers\CommentController@store');
        Route::patch('/posts/{post}/comments/{comment}', 'App\Http\Controllers\CommentController@update');
        Route::delete('/posts/{post}/comments/{comment}', 'App\Http\Controllers\CommentController@destroy');
        //User
        Route::post('/logout', 'App\Http\Controllers\Auth\ApiAuthController@logout')->name('logout.api');
    });
});

