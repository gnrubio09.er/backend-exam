<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Carbon;
use Auth;
use App\Libraries\returnMessage;

class ApiAuthController extends Controller
{
    protected $oReturnMessage;

    /**
     * First function that executes when the ApiAuthController is initialized
     * @param App\Libraries\ReturnMessage $oReturnMessage
     */ 
    public function __construct(ReturnMessage $oReturnMessage)
    {
        $this->oReturnMessage = $oReturnMessage;
    }
    /**
     * Function that stores the data of user
     * @param Illuminate\Http\Request $oRequest
     * @return \Illuminate\Http\Response
     * 
     */
    public function register (Request $oRequest) 
    {
        $oValidator = Validator::make($oRequest->all(), [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|string|min:6|confirmed',
        ]);
        if ($oValidator->fails()){
            $aResponse = $this->oReturnMessage::ifErrorsExists($oValidator->errors()->messages());
            return response($aResponse, 422);
        }
        $oRequest['password']        =   Hash::make($oRequest['password']);
        $oRequest['remember_token']  =   Str::random(10);
        $oUser   = User::create($oRequest->toArray());
        return response($oUser, 200); 
    }
    /**
     * Function that attempts to retrieve a token once credentials is validated
     * @param Illuminate\Http\Request $oRequest
     * @return \Illuminate\Http\Response
     * 
     */
    public function login (Request $oRequest) 
    {
        $oValidator = Validator::make($oRequest->all(), [
            'email'     => 'required|string|email|max:255',
            'password'  => 'required|string|min:6',
        ]);

        if ($oValidator->fails()){
            $aResponse = $this->oReturnMessage::ifErrorsExists($oValidator->errors()->messages());
            return response($aResponse, 422);
        }

        $oUser = User::where('email', $oRequest->email)->first();

        if ($oUser) {
            if (Hash::check($oRequest->password, $oUser->password)) {
                $oToken = $oUser->createToken('Token Granted to Client')->accessToken;
                $aResponse = [
                    'token' => $oToken,
                    'token_type' => 'bearer',
                    'expires_at' => Carbon::now()->addDays(1)
                ];

                return response($aResponse, 200)->header('Authorization', 'Bearer'.$oToken);
            } else {
                $aResponse = $this->oReturnMessage::ifErrorsExists(['password' => ['Password does not match. ']]);
                return response($aResponse, 422);
            }
        } else {
            $aResponse = $this->oReturnMessage::ifErrorsExists(['email' => ['These credentials do not match our records.']]);
            return response($aResponse, 422);
        }
    }
    /**
     * Function that revokes the credentials stored in sessions and token
     * @param Illuminate\Http\Request $oRequest
     * @return \Illuminate\Http\Response
     * 
     */
    public function logout (Request $oRequest) {
        $oToken = $oRequest->user()->token();
        $oToken->revoke();
        return response('Successfully Logged Out!',200)->header('Accept','application/json');
    }

}
