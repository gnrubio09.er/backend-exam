<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Libraries\ReturnMessage;
use Validator;

class CommentController extends Controller
{
    protected $oReturnMessage;

    public function __construct(ReturnMessage $oReturnMessage)
    {
        $this->oReturnMessage = $oReturnMessage;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($sSlug)
    {
        $oComment['data'] = Comment::index($sSlug);
        if(!$oComment['data']) {
            return response(['message'=>"No query results for model [App\\Post]."],404);
        }
        return response($oComment, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param String $sSlug
     * @param \Illuminate\Http\Request $oRequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $oRequest,$sSlug)
    {
        $oValidator = Validator::make($oRequest->all(), ['body'=>'required|string|max:255']);
        if($oValidator->fails()) {
            $aResponse = $this->oReturnMessage::ifErrorsExists($oValidator->errors()->messages());
            return response($aResponse, 422);
        }
        $oComment['data'] = Comment::createComment($oRequest->all(), $sSlug);

        if(!$oComment['data']) {
            return response(['message'   => 'No query results for model [App\\Models\\Comment]',
                             'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException'],404);
        }
        return response($oComment,201);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   string  $sSlug
     * @param   integer $iCommentId
     * @return \Illuminate\Http\Response
     */
    public function update($sSlug, $iCommentId, Request $oRequest)
    {
        $oValidator = Validator::make($oRequest->all(), ['body'=>'required|string|max:255']);
        if($oValidator->fails()) {
            $aResponse = $this->oReturnMessage::ifErrorsExists($oValidator->errors()->messages());
            return response($aResponse, 422);
        }
        $oRequest['comment_id'] = $iCommentId;
        $oComment['data'] = Comment::updateComment($oRequest->all());
        if(!$oComment['data']) {
            return response(['message'   => 'No query results for model [App\\Models\\Comment]',
                             'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException'],404);
        }
        return response($oComment,200)->withHeaders(['Accept' => 'application/json',
                                                     'Content-Type' => 'application/json']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String $sSlug
     * @param  Integer $iCommentId
     * @return \Illuminate\Http\Response
     */
    public function destroy($sSlug, $iCommentId)
    {
        $mResponse = Comment::deleteComment($iCommentId);
        if($mResponse == 0) {
            return response(['message'   => 'No query results for model [App\\Models\\Comment]'],404);
        }
        return response(['status' => 'record deleted successfully'],200);
    }
}
