<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Validator;
use Auth;
use App\Libraries\ReturnMessage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $oPosts = Post::index();
        if(!$oPosts) {
            return response(['message'=>"No query results for model [App\\Post]."],404);
        }
        return response($oPosts,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $oRequest
     * @return \Illuminate\Http\Response
     */
    public function store(Request $oRequest)
    {
        return $this->manipulateData($oRequest);
    }

    /**
     * Display the specified resource.
     *
     * @param  String  $sSlug
     * @return \Illuminate\Http\Response
     */
    public function show($sSlug)
    {
        $oPost['data'] = Post::show($sSlug);
        if(!$oPost['data']) {
            return response(['message'=>"No query results for model [App\\Post]."],404);
        }
        return response($oPost,200);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $oRequest
     * @param  String  $sSlug
     * @return \Illuminate\Http\Response
     */
    public function update(Request $oRequest, $sSlug)
    {
        return $this->manipulateData($oRequest, $sSlug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  String  $sSLug
     * @return \Illuminate\Http\Response
     */
    public function destroy($sSlug)
    {
        $mResponse = Post::deletePost($sSlug);
        if($mResponse == 0) {
            return response(['message'   => 'No query results for model [App\\Models\\Comment]'],404);
        }
        return response(['status' => 'record deleted successfully'],200);
    }

    /**
     * Function that manipulates the data for creating a new Post Model
     * and updating the existing Post Model
     * 
     * @param Object $oRequest
     * @param String $sSlug
     */
    private function manipulateData($oRequest, $sSlug = null)
    {
        $aRules =  ['content'  =>'required|string|max:255',
                    'title'    =>'required|unique:posts|string|max:255',
                    'image'    =>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'];
        $oValidator = Validator::make($oRequest->all(), $aRules);
        if($oValidator->fails()) {
            $aResponse = ReturnMessage::ifErrorsExists($oValidator->errors()->messages());
            return response($aResponse, 422);
        }
        $aPosts = $oRequest->input();
        if($oRequest->hasFile('image') == true) {
            $oImage = $oRequest->file('image');
            $sNewName = 'IMG_'.rand().'.jpg';
            $oImage->move('images/uploads/posts/'.$sNewName);
            $aPosts['content'] = $oRequest->input('content').'</br><img src="../images/uploads/posts/'.$sNewName.'" width="500" />';
        }
        $aPosts['slug'] = str_replace(' ', '-', preg_replace('/[^A-Za-z0-9\ ]/', '', strtolower($aPosts['title']))); // Removes special chars and spaces for slug.
        if(isset($sSlug) == false){
            $iStatus = 201;
            $aPosts['user_id'] = Auth::user()->id;
            $oComment['data'] = Post::createPost($aPosts);
        } else {
            $iStatus = 200;
            unset($aPosts['_method']);
            $oComment['data'] = Post::updatePost($aPosts, $sSlug);
        }
        if(!$oComment['data']) {
            return response(['message'   => 'No query results for model [App\\Models\\Comment]',
            'exception' => 'Symfony\\Component\\HttpKernel\\Exception\\NotFoundHttpException'],404);
        }
        return response($oComment,$iStatus);
    }
}
