<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ForceJsonResponse
{
    /**
     * Handle an incoming request and convert all responses to JSON.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($oRequest, Closure $next)
    {

        return $next($oRequest)->withHeaders(['Accept' => 'application/json',
                                            'Content-Type' => 'application/json']);
    }
}
