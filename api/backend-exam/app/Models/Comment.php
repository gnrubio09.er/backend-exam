<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;
use Auth;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['body','commentable_id','commentable_type','title','creator_id'];
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:00';
    
    public static function index($sSlug)
    {   
        $sPost = Post::show($sSlug);
        if(!$sPost) {
            return false;
        }
        $sCommentableId = $sPost['id'];
        $oComment = Comment::where('commentable_id', $sCommentableId)->get();
        if($oComment->isEmpty()) {
            return false;
        }
        return $oComment;
    }

    public static function createComment($aData, $sSlug)
    {
        $sPost = Post::show($sSlug);
        if(!$sPost) {
            return false;
        }
        $aData['commentable_type'] = array_key_exists('commentable_type',$aData) ? $aData['commentable_type'] : 'App\\Post';
        $aData['commentable_id'] = $sPost['id'];
        $aData['creator_id'] = Auth::user()->id;
        $oComment = Comment::create($aData);
        if(!$oComment) {
            return false;
        }
        return $oComment;
    }

    public static function updateComment($aData)
    {
        $oResponse = Comment::where('id',$aData['comment_id'])->update(['body' => $aData['body']]);
        if(!$oResponse) {
            return false;
        }
        $oResponse = Comment::where('id',$aData['comment_id'])->first();
        if(!$oResponse) {
            return false;
        }
        return $oResponse;
    }

    public static function deleteComment($iCommentId)
    {
        $bResponse = Comment::where('id', $iCommentId)->delete();
        if(!$bResponse) {
            return 0;
        }
        return 1;
    }
}
