<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon;

class Post extends Model
{
    use HasFactory;    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'slug', 'user_id'];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:00';

    public static function index()
    {
        $oPosts = Post::where('deleted_at', null)->paginate(10);
        if(!$oPosts) {
            return false;
        }
        $oResponse['data'] = $oPosts->getCollection();
        $oResponse['links']['first'] = url()->current().'?page=1';
        $oResponse['links']['last'] = url()->current().'?page='.$oPosts->lastPage();
        $oResponse['links']['prev'] = $oPosts->previousPageUrl();
        $oResponse['links']['next'] = $oPosts->nextPageUrl();
        $oResponse['meta']['current_page'] = $oPosts->currentPage();
        $oResponse['meta']['from'] = $oPosts->firstItem();
        $oResponse['meta']['last_page'] = $oPosts->lastPage();
        $oResponse['meta']['path'] = url()->current();
        $oResponse['meta']['per_page'] = $oPosts->perPage();
        $oResponse['meta']['to'] = $oPosts->lastItem();
        $oResponse['meta']['total'] = $oPosts->total();
        return $oResponse;
    }

    public static function show($sSlug)
    {
        $oPost = Post::where('slug', $sSlug)->first();
        if(!$oPost) {
            return false;
        }
        return $oPost;
    }

    public static function createPost($aData)
    {
        $oResponse = Post::create($aData);
        if(!$oResponse) {
            return 0;
        }
        return $oResponse;
    }

    public static function updatePost($aData, $sSlug)
    {
        $oResponse = Post::where('slug', $sSlug)->update($aData);
        if(!$oResponse) {
            return false;
        }
        $oResponse = Post::where('slug',$aData['slug'])->first();
        if(!$oResponse) {
            return false;
        }
        return $oResponse;
    }

    public static function deletePost($sSlug)
    {
        $bResponse = Post::where('slug', $sSlug)->update(['deleted_at' => Carbon::now()]);
        if(!$bResponse) {
            return 0;
        }
        return 1;
    }
}
