<?php

namespace App\Libraries;

class ReturnMessage
{
    public static function ifErrorsExists($aErrors) {
        $aErrorMessages = [];
        foreach ($aErrors as $sKey => $sValue) {
            $aErrorMessages[$sKey] = $sValue;
        }

        return [ 
            'message'   => 'The given data was invalid.',
            'errors'    =>  $aErrorMessages
        ];

    }

}
