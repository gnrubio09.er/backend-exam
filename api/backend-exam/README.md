
 <h1 align="center">RESTful API Installation</h1>



<p align="center">
I used Laravel Framework in developing this code. In order for you to experience the process that I did,
simply use the following below:<br><br>
</p>


* [PHP 7.4.5 or any 7+ versions](https://windows.php.net/downloads/releases/php-7.4.14-src.zip)<br>
* [Node JS v12.16.3](https://nodejs.org/dist/v12.16.3/node-v12.16.3-x64.msi)<br>
* [NPM v6.14.4](https://nodejs.org/en/download/)<br>
* [MySQL 8 ](https://www.mysql.com/downloads/)<br>
* [Composer version 2.0.8 ](https://getcomposer.org/Composer-Setup.exe)<br>
* [Git Version Control v2.26 ](https://git-scm.com/download/win)<br>
* [Laravel Passport](https://laravel.com/docs/7.x/passport#installation)


<h2 align="center">Let's Start!</h2>
<ol>
    <li>Create a database/schema in your server.</li>
    <li>Duplicate the .env.example file, then rename it to .env (But other versions of Laravel were already creating it's own Environment file)</li>
    <li>Write your schema in the .env file, then open a Terminal (Bash, Command Prompt, etc.,)</li>
    <li>Open a terminal in the directory of your project</li>
    <li>Run "composer install" </li>
    <li>Run "php artisan key:generate" </li>
    <li>Run "php artisan:migrate"</li>
    <li>Run "php artisan passport:install"</li>
    <li>Laravel helpers (for Laravel 6.0 and up), just run "composer require laravel/helpers"</li>
    <li>Lastly, run "php artisan serve" for the backend to run.</li>
</ol>



