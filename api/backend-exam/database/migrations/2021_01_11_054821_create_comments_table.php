<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('body');
            $table->string('commentable_type')->default('App\\\Post');
            $table->integer('commentable_id')->unsigned();
            $table->unsignedBigInteger('creator_id');
            $table->string('parent_id')->nullable();
            $table->timestamps();

            $table->foreign('commentable_id')
            ->references('id')
            ->on('posts')
            ->onDelete('cascade');

            $table->foreign('creator_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
